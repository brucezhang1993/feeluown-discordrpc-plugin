# FeelUOwn Discord Rich Presence
这是一个实现 Discord RPC Rich Precense 服务的 FeelUOwn 插件

### 关于 What are these
- [What is Discord](https://discordapp.com/)
- [What is Rich Precense](https://discordapp.com/rich-presence)
- [What is FeelUOwn](https://github.com/cosven/FeelUOwn)

### 食用攻略 Usage

1. 安装并第一次运行 FeelUOwn, 初始化文件夹
2. 打开 Linux 终端并 `cd $HOME/.FeelUOwn/plugins`
3. 安装插件 master 分支开发版
```shell
git clone https://github.com/BruceZhang1993/feeluown-discordrpc-plugin.git
```

### 插件更新 Upgrade

```shell
cd $HOME/.FeelUOwn/plugins/feeluown-discordrpc-plugin
git pull origin master
```

### 特性 Features

- 实时显示当前播放歌曲信息（歌曲名/演唱者等）
- 显示当前曲目播放剩余时间及播放进度
- 更多信息显示和插件配置支持有待开发
- 与 Discord RPC 的简单自动重连机制

### 反馈 Feedback

- 与插件有关建议与反馈请发 Issue
- 与 FeelUOwn 本体有关建议与反馈请前往 [FeelUOwn Issue](https://github.com/cosven/FeelUOwn/issues)

### 开源协议 License

FeelUOwn Discord Rich Presence Plugin: [MIT](https://github.com/BruceZhang1993/feeluown-discordrpc-plugin/blob/master/LICENSE)

FeelUOwn: [GPL3](https://github.com/cosven/FeelUOwn/blob/master/LICENSE)
